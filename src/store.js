import Vue from 'vue';
import Vuex from 'vuex';

import User from './models/User';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    user: new User(),
  },
  mutations: {
    login(state, userInfo) {
      state.user = new User(userInfo);
    },
    logoff(state) {
      state.user = null;
    },
  },
  getters: {
    fullName: state => `${state.user.firstName} ${state.user.lastName}`,
  },
  actions: {
  },
});
