export default class {
  constructor(args = {}) {
    Object.assign(this, {
      id: '',
      brand: '',
      model: '',
      state: 0,
      nbDoors: 0,
      rate: 5,
      priceRent: 0,
      nbRequester: 0,
      rented: false,
      requestPending: false,
    }, args);
  }
}
