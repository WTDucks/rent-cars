export default class {
  constructor(args = {}) {
    Object.assign(this, {
      email: '',
      lastName: '',
      firstName: '',
      age: '',
      service: '',
      id: -1,
    }, args);
  }

  get isEmpty() {
    return this.email === '';
  }
}
