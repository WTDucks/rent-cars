import Vue from 'vue';
import Router from 'vue-router';
import Login from './views/Login.vue';
import Home from './views/Home.vue';
import Rent from './views/Rent.vue';
import UserRequest from './components/UserRequests.vue';
import store from './store';

Vue.use(Router);

const router = new Router({
  routes: [
    {
      path: '*',
      component: Login,
    },
    {
      path: '/',
      component: Login,
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
    },
    {
      path: '/home',
      name: 'home',
      component: Home,
      meta: {
        requiresAuth: true,
      },
      children: [
        {
          path: 'rent',
          name: 'rent',
          component: Rent,
        },
        {
          path: 'requests',
          name: 'requests',
          component: UserRequest,
        },
      ],
    },
  ],
});

router.beforeEach((to, from, next) => {
  const isLogged = !store.state.user.isEmpty;
  const requiredAuth = to.matched.some(record => record.meta.requiresAuth);

  if (requiredAuth && !isLogged) next('login');
  else if (!requiredAuth && isLogged) next('home');
  else next();
});

export default router;
